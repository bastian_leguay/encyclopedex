package com.example.encyclopedex;

import androidx.appcompat.app.AppCompatActivity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.os.Bundle;

public class SendMail extends AppCompatActivity {

    EditText editText;
    String getEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_mail);
        Button startBtn = (Button) findViewById(R.id.sendEmail);

        editText = (EditText) findViewById(R.id.editText);
        getEditText = editText.getText().toString();
        startBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                sendEmail();
            }
        });
    }




    protected void sendEmail() {
        Log.i("Envoi un mail", "");
        String[] TO = {""};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Ce message vient de l'application Encyclopedex");
        emailIntent.putExtra(Intent.EXTRA_TEXT, editText.getText().toString());

        try {
            startActivity(Intent.createChooser(emailIntent, "Envoie du mail"));
            finish();
            Log.i("Mail envoyer", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(SendMail.this, "Il y a pas de support de message", Toast.LENGTH_SHORT).show();
        }
    }

}
