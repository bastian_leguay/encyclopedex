package com.example.encyclopedex;

import java.util.Comparator;

public class LifespanComparator implements Comparator<Animal> {

        @Override
        public int compare(Animal e1, Animal e2) {
            return e1.getLifespan().compareTo(e2.getLifespan());
        }
}
