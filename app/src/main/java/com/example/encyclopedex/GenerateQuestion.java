package com.example.encyclopedex;

public class GenerateQuestion {
    private Animal animal1, animal2;

    public GenerateQuestion(Animal animal1, Animal animal2) {
        this.animal1 = animal1;
        this.animal2 = animal2;
    }
    public String randomQuestion(){
        String res = "";
        switch ((int) (Math.random() * 3)){
            case 0:
                res = "Qui est le plus grand ?";
                break;
            case 1:
                res = "Qui est le plus lourd ?";
                break;
            case 2:
                res = "Qui vie le plus longtemps ?";
                break;
        }
        return res;
    }
}
