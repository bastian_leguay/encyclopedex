package com.example.encyclopedex;

import java.util.Comparator;

public class WeightComparator implements Comparator<Animal> {

        @Override
        public int compare(Animal e1, Animal e2) {
            return e1.getWeight_max().compareTo(e2.getWeight_max());
        }
}
