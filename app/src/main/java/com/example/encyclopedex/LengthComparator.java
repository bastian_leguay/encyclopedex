package com.example.encyclopedex;

import java.util.Comparator;

public class LengthComparator implements Comparator<Animal> {

        @Override
        public int compare(Animal e1, Animal e2) {
            return e1.getLength_max().compareTo(e2.getLength_max());
        }
}
