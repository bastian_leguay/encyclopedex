package com.example.encyclopedex;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.encyclopedex.bd.DBHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Quizz#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Quizz extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
        private static final DBHandler ARG_PARAM2 = null;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private DBHandler db;
    private Animal animal1, animal2;

    private ImageView image1, image2;
    private TextView question, rep1, rep2;

    private String name;
    private String latin_name;
    private String animal_type;
    private Float length_min;
    private Float length_max;
    private Float weigth_min;
    private Float weigth_max;
    private Float lifespan;
    private String habitat;
    private String diet;
    private String geo_range;
    private String image_link;

    public Quizz() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
//     * @param param1 Parameter 1.
//     * @param param2 Parameter 2.
     * @return A new instance of fragment Quizz.
     */
    // TODO: Rename and change types and number of parameters
    public static Quizz newInstance(String param1) {
        Quizz fragment = new Quizz();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            this.db = new DBHandler(getContext());
        }
        MyHTTPRequest rt = new MyHTTPRequest();
        rt.execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_quizz, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

            TextView tQuizz = getView().findViewById(R.id.tQuizz);
            tQuizz.setText(mParam1);
            image1 = getView().findViewById(R.id.Image1);
            image2 = getView().findViewById(R.id.Image2);
            question = getView().findViewById(R.id.Question);
            rep1 = getView().findViewById(R.id.Response1);
            rep2 = getView().findViewById(R.id.Response2);
    }

//    /**
//    * Génère un textview contenant une question
//    * @param question la question
//    * @param index l'id
//    * @param layout le layout dans lequel on va inclure ce textview
//    */
//    private void generateTextViewQuestion (String question, String rep1, String rep2, int index, LinearLayout layout){
//    TextView t;
//    t = new TextView(getApplicationContext());
//    t.setText(question);
//    t.setId(index);
//    layout.addView(t);
//    }
    public class MyHTTPRequest extends AsyncTask<Activity, Void, String> {
        Activity activity=null;
        private List<Animal> l = new ArrayList<>();
        @Override
        // Le corps de la tâche asynchrone (exécuté en tâche de fond)
        //  lance la requète
        protected String doInBackground(Activity... params) {
//            activity=params[0];
            String response = requete();
            // ... Exécution de la requète et appel de decodeJSON
            return response;
        }
        private String requete() {
            String response = "";
            try {
            HttpURLConnection connection = null;
            URL url = new
            URL("https://zoo-animal-api.herokuapp.com/animals/rand/2");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            InputStream inputStream = connection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String ligne = bufferedReader.readLine() ;
            while (ligne!= null){
            response+=ligne;
            ligne = bufferedReader.readLine();
            }
                JSONArray toDecode = new JSONArray(response);
                response = decodeJSON(toDecode);
            } catch (UnsupportedEncodingException e) {
            response = "problème d'encodage";
            } catch (MalformedURLException e) {
            response = "problème d'URL ";
            } catch (IOException e) {
            response = "problème de connexion ";
            } catch (Exception e) {
            e.printStackTrace();
            }
            return response;
        }
        private String decodeJSON(JSONArray js) throws Exception {
            String response = "";
            for(int i = 0 ; i<js.length(); i++){
                JSONObject animal = js.getJSONObject(i);
                name = animal.getString("name");
                latin_name = animal.getString("latin_name");
                animal_type = animal.getString("animal_type");
                length_min = Float.valueOf(animal.getString("length_min"));
                length_max = Float.valueOf(animal.getString("length_max"));
                weigth_min = Float.valueOf(animal.getString("weight_min"));
                weigth_max = Float.valueOf(animal.getString("weight_max"));
                lifespan = Float.valueOf(animal.getString("lifespan"));
                habitat = animal.getString("habitat");
                diet = animal.getString("diet");
                geo_range = animal.getString("geo_range");
                image_link = animal.getString("image_link");
                l.add(new Animal(name, latin_name, animal_type, habitat, diet, geo_range, image_link, length_min, length_max, weigth_min, weigth_max, lifespan));
                response += "name:" + name;
            }
            return response;
        }
        public List<String> randomQuestion(){
            List<String> res = new ArrayList<>();
            Animal answer;
            switch ((int) (Math.random() * 3)){
                case 0:
                    res.add("Qui est le plus grand ?");
//                    res.add(String.valueOf(animal1.getLength_max()));
//                    res.add(String.valueOf(animal2.getLength_max()));
                    answer = Collections.max(Arrays.asList(animal1,  animal2), new LengthComparator());
                    res.add(answer.getName());
                    res.add(String.valueOf(answer.getLength_max()));
                    break;
                case 1:
                    res.add("Qui est le plus lourd ?");
//                    res.add(String.valueOf(animal1.getWeight_max()));
//                    res.add(String.valueOf(animal2.getWeight_max()));
                    answer = Collections.max(Arrays.asList(animal1,  animal2), new WeightComparator());
                    res.add(answer.getName());
                    res.add(String.valueOf(answer.getWeight_max()));
                    break;
                case 2:
                    res.add("Qui vie le plus longtemps ?");
//                    res.add(String.valueOf(animal1.getLifespan()));
//                    res.add(String.valueOf(animal2.getLifespan()));
                    answer = Collections.max(Arrays.asList(animal1,  animal2), new LifespanComparator());
                    res.add(answer.getName());
                    res.add(String.valueOf(answer.getLifespan()));
                    break;
            }
            return res;
        }
        // Méthode appelée lorsque la tâche de fond sera terminée
        //  Affiche le résultat
        protected void onPostExecute(String result) {
            Log.i("Requete", result);
            animal1 = l.get(0);
            animal2 = l.get(1);
            new DownloadImageTask(image1).execute(animal1.getImage_link());
            new DownloadImageTask(image2).execute(animal2.getImage_link());
            List<String> rdQuestion = randomQuestion();
            question.setText(rdQuestion.get(0));
            rep1.setText(animal1.getName());
            rep2.setText(animal2.getName());
            Button button1 = (Button) getView().findViewById(R.id.Response1);
            Button button2 = (Button) getView().findViewById(R.id.Response2);
            TextView res = getView().findViewById(R.id.res);
            View.OnClickListener t = new View.OnClickListener()
               {
                     @Override
                     public void onClick(View v)
                     {
                        Button b = (Button) v;
                        if(b.getText() == rdQuestion.get(1)){
                            db.insertAnimal(name, latin_name, animal_type, String.valueOf(length_min), String.valueOf(length_max), String.valueOf(weigth_min), String.valueOf(weigth_max), String.valueOf(lifespan), habitat, diet, geo_range, image_link);
                            res.setTextColor(Color.BLUE);
                            res.setText("Bonne reponse !");
                        }
                        else {
                            res.setTextColor(Color.RED);
                            res.setText("Mauvaise reponse !");
                        }
                        button1.setOnClickListener(null);
                        button2.setOnClickListener(null);
                     }
               };
               button1.setOnClickListener(t);
               button2.setOnClickListener(t);
               Button reset = getView().findViewById(R.id.reset);
                reset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getActivity().recreate();
                    }
                });

//            image.setImageURI(Uri.parse(animal1.getImage_link()));
//            generateTextViewQuestion(result, 1, tResultat);
        }
        }
        private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
          ImageView bmImage;

          public DownloadImageTask(ImageView bmImage) {
              this.bmImage = bmImage;
          }

          protected Bitmap doInBackground(String... urls) {
              String urldisplay = urls[0];
              Bitmap mIcon11 = null;
              try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
              } catch (Exception e) {
                  Log.e("Error", e.getMessage());
                  e.printStackTrace();
              }
              return mIcon11;
          }

          protected void onPostExecute(Bitmap result) {
              bmImage.setImageBitmap(result);
          }
        }
    }