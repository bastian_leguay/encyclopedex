package com.example.encyclopedex;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.encyclopedex.bd.DBHandler;
import com.example.encyclopedex.bd.Response;

import java.io.InputStream;
import java.util.List;
import java.util.Locale;

public class DetailleAnimaux extends AppCompatActivity {

private DBHandler db;
private LinearLayout ll;
private ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detaille_animaux);
        ll = (LinearLayout) findViewById(R.id.detailleAnimaux);
        db = new DBHandler(getApplicationContext());
        List<Response> data = db.selectAll();



        Intent i= getIntent();
        for (Response d:data) {


            if(d.getM_id() == i.getIntExtra("animal_Id",0)){

                TextView tNom = new TextView(getApplicationContext());
                tNom.setId(getTaskId());
                tNom.setText("Nom de l'animal :"+d.getName());
                ll.addView(tNom);

                TextView tNom_Latin = new TextView(getApplicationContext());
                tNom_Latin.setId(getTaskId());
                tNom_Latin.setText("Nom Latin de l'animal :"+d.getLatin_name());
                ll.addView(tNom_Latin);

                TextView tAnimal_type = new TextView(getApplicationContext());
                tAnimal_type.setId(getTaskId());
                tAnimal_type.setText("Espece de l'animal :"+d.getAnimal_type());
                ll.addView(tAnimal_type);

                TextView tLength_min = new TextView(getApplicationContext());
                tLength_min.setId(getTaskId());
                String s = ""+d.getLength_min();
                tLength_min.setText("Taille minimum de l'animal :"+s);
                ll.addView(tLength_min);

                TextView tLength_max = new TextView(getApplicationContext());
                tLength_max.setId(getTaskId());
                String s2 = ""+d.getLength_max();
                tLength_max.setText("Taille maximum de l'animal :"+s2);
                ll.addView(tLength_max);

                TextView tWeight_min = new TextView(getApplicationContext());
                tWeight_min.setId(getTaskId());
                String s3 = ""+d.getWeight_min();
                tWeight_min.setText("Poid minimum de l'animal :"+s3);
                ll.addView(tWeight_min);

                TextView tWeight_max = new TextView(getApplicationContext());
                tWeight_max.setId(getTaskId());
                String s4 = ""+d.getWeight_max();
                tWeight_max.setText("Poid Maximum de l'animal :"+s4);
                ll.addView(tWeight_max);

                TextView tLife = new TextView(getApplicationContext());
                tLife.setId(getTaskId());
                String s5 = ""+d.getLifespan();
                tLife.setText("Durée de vie de l'animal :"+s5);
                ll.addView(tLife);

                TextView tHabitat = new TextView(getApplicationContext());
                tHabitat.setId(getTaskId());
                tHabitat.setText("Habitat de l'animal :"+d.getHabitat());
                ll.addView(tHabitat);

                TextView tDiet = new TextView(getApplicationContext());
                tDiet.setId(getTaskId());
                tDiet.setText("Régime alimentaire de l'animal :"+d.getDiet());
                ll.addView(tDiet);

                TextView tGeo = new TextView(getApplicationContext());
                tGeo.setId(getTaskId());
                tGeo.setText("Lieu géographique de l'animal :"+d.getGeo_range());
                ll.addView(tGeo);

                ImageView img = new ImageView(getApplicationContext());
                ll.addView(img);
                new DetailleAnimaux.DownloadImageTask(img).execute(d.getImage_link());


            }





        }
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
    private void ajoutImage(){
        ll.addView(image);
    }
}