package com.example.encyclopedex;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewbinding.ViewBindings;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.example.encyclopedex.bd.DBContract;
import com.example.encyclopedex.bd.DBHandler;
import com.example.encyclopedex.bd.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link All#newInstance} factory method to
 * create an instance of this fragment.
 */
public class All extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final DBHandler ARG_PARAM2 = null;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private DBHandler db;
    private LinearLayout ll;

    private TextView textV;
    private Animal animal;

    private ImageView image;
    private String name;
    private String image_link;


    public All() {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * <p>
     * //     * @param param1 Parameter 1.
     * //     * @param param2 Parameter 2.
     *
     * @return A new instance of fragment Quizz.
     */
    // TODO: Rename and change types and number of parameters
    public static All newInstance(String param1) {
        All fragment = new All();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

        }

            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_all, container, false);

    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        ll = (LinearLayout) view.findViewById(R.id.animaux);
        db = new DBHandler(getActivity());
        generateData();

    }
    public void generateData(){
        List<Response> data = db.selectAll();
        for (Response d:data){

            Button button = new Button(getActivity().getApplicationContext());
            button.setText(d.getName());
            button.setId(getId());

            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {

                    Intent intent = new Intent(getActivity(), DetailleAnimaux.class);
                    intent.putExtra("animal_Id",d.getM_id());
                    startActivity(intent);

                }
            });

            ImageView img = new ImageView(getActivity());
            ll.addView(img);
            new All.DownloadImageTask(img).execute(d.getImage_link());
            ll.addView(button);
        }
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

          protected void onPostExecute(Bitmap result) {
              bmImage.setImageBitmap(result);
          }
    }
    private void ajoutImage(){
        ll.addView(image);
    }

}