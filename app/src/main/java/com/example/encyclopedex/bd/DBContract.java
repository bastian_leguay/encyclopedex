package com.example.encyclopedex.bd;

public final class DBContract {

    public static class Form {
        public static final String TABLE_NAME = "animal";
        public static final String _ID = "id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_LATIN_NAME = "latin_name";
        public static final String COLUMN_ANIMAL_TYPE = "animal_type";
        public static final String COLUMN_LENGTH_MIN = "length_min";
        public static final String COLUMN_LENGTH_MAX = "length_max";
        public static final String COLUMN_WEIGHT_MIN = "weight_min";
        public static final String COLUMN_WEIGHT_MAX = "weight_max";
        public static final String COLUMN_LIFESPAN = "lifespan";
        public static final String COLUMN_HABITAT = "habitat";
        public static final String COLUMN_DIET = "diet";
        public static final String COLUMN_GEO_RANGE = "geo_range";
        public static final String COLUMN_IMAGE_LINK = "image_link";

    }

}
