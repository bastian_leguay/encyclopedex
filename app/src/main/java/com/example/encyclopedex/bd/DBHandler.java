package com.example.encyclopedex.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DBHandler extends SQLiteOpenHelper {
    //change version when upgraded
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Form.db";

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String query =  "CREATE TABLE " + DBContract.Form.TABLE_NAME + " (" +
                DBContract.Form._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                DBContract.Form.COLUMN_NAME + " TEXT," +
                DBContract.Form.COLUMN_LATIN_NAME + " TEXT," +
                DBContract.Form.COLUMN_ANIMAL_TYPE + " TEXT," +
                DBContract.Form.COLUMN_LENGTH_MIN + " FLOAT," +
                DBContract.Form.COLUMN_LENGTH_MAX + " FLOAT," +
                DBContract.Form.COLUMN_WEIGHT_MIN + " FLOAT," +
                DBContract.Form.COLUMN_WEIGHT_MAX + " FLOAT," +
                DBContract.Form.COLUMN_LIFESPAN + " FLOAT," +
                DBContract.Form.COLUMN_HABITAT + " TEXT," +
                DBContract.Form.COLUMN_DIET + " TEXT," +
                DBContract.Form.COLUMN_GEO_RANGE + " TEXT," +
                DBContract.Form.COLUMN_IMAGE_LINK + " TEXT)";
        db.execSQL(query);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = "DROP TABLE IF EXISTS " + DBContract.Form.TABLE_NAME;
        onCreate(db);
    }

    public void insertAnimal(String name, String latin_name, String animal_type, String length_min, String length_max, String weight_min, String weight_max, String lifespan, String habitat, String diet, String geo_range, String image_link){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues row = new ContentValues();
        row.put(DBContract.Form.COLUMN_NAME,name);
        row.put(DBContract.Form.COLUMN_LATIN_NAME,latin_name);
        row.put(DBContract.Form.COLUMN_ANIMAL_TYPE,animal_type);
        row.put(DBContract.Form.COLUMN_LENGTH_MIN,length_min);
        row.put(DBContract.Form.COLUMN_LENGTH_MAX,length_max);
        row.put(DBContract.Form.COLUMN_WEIGHT_MIN,weight_min);
        row.put(DBContract.Form.COLUMN_WEIGHT_MAX,weight_max);
        row.put(DBContract.Form.COLUMN_LIFESPAN,lifespan);
        row.put(DBContract.Form.COLUMN_HABITAT,habitat);
        row.put(DBContract.Form.COLUMN_DIET,diet);
        row.put(DBContract.Form.COLUMN_GEO_RANGE,geo_range);
        row.put(DBContract.Form.COLUMN_IMAGE_LINK,image_link);
        // return row primary key
        long newRowId = db.insert(DBContract.Form.TABLE_NAME,null,row);
    }

    public List<Response> selectAll() {
        SQLiteDatabase db = this.getReadableDatabase();
        // columns to get
        String [] projection = {
        DBContract.Form.COLUMN_NAME,
        DBContract.Form.COLUMN_LATIN_NAME,
        DBContract.Form.COLUMN_ANIMAL_TYPE,
        DBContract.Form.COLUMN_LENGTH_MIN,
        DBContract.Form.COLUMN_LENGTH_MAX,
        DBContract.Form.COLUMN_WEIGHT_MIN,
        DBContract.Form.COLUMN_WEIGHT_MAX,
        DBContract.Form.COLUMN_LIFESPAN,
        DBContract.Form.COLUMN_HABITAT,
        DBContract.Form.COLUMN_DIET,
        DBContract.Form.COLUMN_GEO_RANGE,
        DBContract.Form.COLUMN_IMAGE_LINK,
        DBContract.Form._ID
        };
        Cursor cursor = db.query(
        DBContract.Form.TABLE_NAME,
        projection,
        null,
        null,
        null,
        null,
        null
        );
        List<Response> responses = new ArrayList<>();
        while(cursor.moveToNext()){
            String name = cursor.getString((int)cursor.getColumnIndex(DBContract.Form.COLUMN_NAME));
            String latin_name = cursor.getString((int)cursor.getColumnIndex(DBContract.Form.COLUMN_LATIN_NAME));
            String animal_type = cursor.getString((int)cursor.getColumnIndex(DBContract.Form.COLUMN_ANIMAL_TYPE));
            float length_min = cursor.getFloat((int)cursor.getColumnIndex(DBContract.Form.COLUMN_LENGTH_MIN));
            float length_max = cursor.getFloat((int)cursor.getColumnIndex(DBContract.Form.COLUMN_LENGTH_MAX));
            float weight_min = cursor.getFloat((int)cursor.getColumnIndex(DBContract.Form.COLUMN_WEIGHT_MIN));
            float weight_max = cursor.getFloat((int)cursor.getColumnIndex(DBContract.Form.COLUMN_WEIGHT_MAX));
            float lifespan = cursor.getFloat((int)cursor.getColumnIndex(DBContract.Form.COLUMN_LIFESPAN));
            String habitat = cursor.getString((int)cursor.getColumnIndex(DBContract.Form.COLUMN_HABITAT));
            String diet = cursor.getString((int)cursor.getColumnIndex(DBContract.Form.COLUMN_DIET));
            String geo_range = cursor.getString((int)cursor.getColumnIndex(DBContract.Form.COLUMN_GEO_RANGE));
            String image_link = cursor.getString((int)cursor.getColumnIndex(DBContract.Form.COLUMN_IMAGE_LINK));
            int id = cursor.getInt((int)cursor.getColumnIndex(DBContract.Form._ID));
            Response tmp = new Response (name, latin_name, animal_type, length_min, length_max, weight_min, weight_max, lifespan, habitat, diet, geo_range, image_link, id);
            responses.add(tmp);
        }
        cursor.close();
        return responses;
    }

}

