package com.example.encyclopedex.bd;

public class Response {

    private String name ;
    private String latin_name ;
    private String animal_type ;
    private float length_min ;
    private float length_max ;
    private float weight_min ;
    private float weight_max ;
    private float lifespan ;
    private String habitat ;
    private String diet ;
    private String geo_range ;
    private String image_link ;
    private int m_id;

    public Response(String name, String latin_name, String animal_type, float length_min, float length_max, float weight_min, float weight_max, float lifespan, String habitat, String diet, String geo_range, String image_link, int m_id) {
        this.name = name;
        this.latin_name = latin_name;
        this.animal_type = animal_type;
        this.length_min = length_min;
        this.length_max = length_max;
        this.weight_min = weight_min;
        this.weight_max = weight_max;
        this.lifespan = lifespan;
        this.habitat = habitat;
        this.diet = diet;
        this.geo_range = geo_range;
        this.image_link = image_link;
        this.m_id = m_id;
    }

    public Response(String name, String img) {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatin_name() {
        return latin_name;
    }

    public void setLatin_name(String latin_name) {
        this.latin_name = latin_name;
    }

    public String getAnimal_type() {
        return animal_type;
    }

    public void setAnimal_type(String animal_type) {
        this.animal_type = animal_type;
    }

    public float getLength_min() {
        return length_min;
    }

    public void setLength_min(float length_min) {
        this.length_min = length_min;
    }

    public float getLength_max() {
        return length_max;
    }

    public void setLength_max(float length_max) {
        this.length_max = length_max;
    }

    public float getWeight_min() {
        return weight_min;
    }

    public void setWeight_min(float weight_min) {
        this.weight_min = weight_min;
    }

    public float getWeight_max() {
        return weight_max;
    }

    public void setWeight_max(float weight_max) {
        this.weight_max = weight_max;
    }

    public float getLifespan() {
        return lifespan;
    }

    public void setLifespan(float lifespan) {
        this.lifespan = lifespan;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    public String getDiet() {
        return diet;
    }

    public void setDiet(String diet) {
        this.diet = diet;
    }

    public String getGeo_range() {
        return geo_range;
    }

    public void setGeo_range(String geo_range) {
        this.geo_range = geo_range;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public int getM_id(){ return m_id;}
}
