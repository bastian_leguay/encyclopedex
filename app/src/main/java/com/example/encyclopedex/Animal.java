package com.example.encyclopedex;

public class Animal {
    private String name, latin_name, animal_type, habitat, diet, geo_range, image_link;
    private Float length_min, length_max, weight_min, weight_max, lifespan;

    public Animal(String name, String latin_name, String animal_type, String habitat, String diet,
                  String geo_range, String image_link, Float length_min, Float length_max,
                  Float weight_min, Float weight_max, Float lifespan) {
        this.name = name;
        this.latin_name = latin_name;
        this.animal_type = animal_type;
        this.habitat = habitat;
        this.diet = diet;
        this.geo_range = geo_range;
        this.image_link = image_link;
        this.length_min = length_min;
        this.length_max = length_max;
        this.weight_min = weight_min;
        this.weight_max = weight_max;
        this.lifespan = lifespan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatin_name() {
        return latin_name;
    }

    public void setLatin_name(String latin_name) {
        this.latin_name = latin_name;
    }

    public String getAnimal_type() {
        return animal_type;
    }

    public void setAnimal_type(String animal_type) {
        this.animal_type = animal_type;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    public String getDiet() {
        return diet;
    }

    public void setDiet(String diet) {
        this.diet = diet;
    }

    public String getGeo_range() {
        return geo_range;
    }

    public void setGeo_range(String geo_range) {
        this.geo_range = geo_range;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public Float getLength_min() {
        return length_min;
    }

    public void setLength_min(Float length_min) {
        this.length_min = length_min;
    }

    public Float getLength_max() {
        return length_max;
    }

    public void setLength_max(Float length_max) {
        this.length_max = length_max;
    }

    public Float getWeight_min() {
        return weight_min;
    }

    public void setWeight_min(Float weight_min) {
        this.weight_min = weight_min;
    }

    public Float getWeight_max() {
        return weight_max;
    }

    public void setWeight_max(Float weight_max) {
        this.weight_max = weight_max;
    }

    public Float getLifespan() {
        return lifespan;
    }

    public void setLifespan(Float lifespan) {
        this.lifespan = lifespan;
    }

}
